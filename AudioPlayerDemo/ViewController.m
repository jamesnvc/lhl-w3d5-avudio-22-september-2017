//
//  ViewController.m
//  AudioPlayerDemo
//
//  Created by James Cash on 22-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;

@interface ViewController ()

@property (nonatomic,strong) NSURL *audioFileURL;
@property (nonatomic,strong) AVAudioRecorder *recorder;
@property (nonatomic,strong) AVAudioPlayer *player;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
//    NSLog(@"Document dir = %@", docPath);
    self.audioFileURL = [[NSURL URLWithString:docPath]
                         URLByAppendingPathComponent:@"recording.m4a"];
//    NSLog(@"Audio file url = %@", self.audioFileURL);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleRecord:(UIButton *)sender {
    if ([self.recorder isRecording]) {
        [self.recorder stop];
        [sender setTitle:@"Record" forState:UIControlStateNormal];
        return;
    }
    [sender setTitle:@"Stop" forState:UIControlStateNormal];

    NSError *err = nil;
    self.recorder = [[AVAudioRecorder alloc]
                     initWithURL:self.audioFileURL
                     settings:@{AVFormatIDKey: @(kAudioFormatMPEG4AAC),
                                AVNumberOfChannelsKey: @(2),
                                AVSampleRateKey: @(44100)}
                     error:&err];
    if (err != nil) {
        NSLog(@"Error creating recorder: %@", err.localizedDescription);
        abort();
    }
    [self.recorder record];
}

- (IBAction)togglePlay:(UIButton *)sender {
    if ([self.player isPlaying]) {
        [self.player stop];
        [sender setTitle:@"Play" forState:UIControlStateNormal];
        return;
    }
    [sender setTitle:@"Stop" forState:UIControlStateNormal];

    NSError *err = nil;
    self.player = [[AVAudioPlayer alloc]
                   initWithContentsOfURL:self.audioFileURL
                   error:&err];
    if (err != nil) {
        NSLog(@"Error creating player: %@", err.localizedDescription);
        abort();
    }

    [self.player play];
}

@end
